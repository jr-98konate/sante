import React from 'react'
import "./Navbar.css";
import logo from "../img/HaiDoc.png"
import Header from './Header';

function Navbar() {
  return (
	<>
		  <section className='section-hed'>
			  <nav className="navbar navbar-expand-lg navbar-white bg-white">
				  <div className="container-fluid">
					  <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
						  <span className="navbar-toggler-icon"></span>
					  </button>
					  <div className="collapse navbar-collapse" id="navbarSupportedContent">
						  <a className="navbar-brand mt-2 mt-lg-0" href="#">
							  <img className='logo' src={logo} alt="logo"

							  />
						  </a>
						  <ul className="navbar-nav mx-auto mb-2 mb-lg-0">
							  <li className="nav-item">
								  <a className="nav-link" href="#">Home</a>
							  </li>
							  <li className="nav-item">
								  <a className="nav-link" href="#">services</a>
							  </li>
							  <li className="nav-item">
								  <a className="nav-link" href="#">Product</a>
							  </li>
							  <li className="nav-item">
								  <a className="nav-link" href="#">About Us</a>
							  </li>
						  </ul>
					  </div>
					  <div className="d-flex align-items-center">
						  <button className='blue' type='button'>Register</button>
					  </div>
				  </div>
			  </nav>
			  <Header />
		  </section>
			
			



	</>
  )
}

export default Navbar