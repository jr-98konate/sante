import React from 'react'
import doctor from "../img/doctor.png"
import img1 from "../img/s1 1.png";
import doctor1 from "../img/doctor1.png";
import docs from "../img/docs.png";
import lunete from "../img/groupel.png";
import groupe132 from "../img/texte.png";
import groupe23 from "../img/groupe23.png";



function Header() {
  return (
	<>
		  <div className='container my-5'>
			  <div className='row'>
				  <div className='col  left'>
					  <h1 className=' text-justify are'>We Are Ready to</h1>
					  <h1 className=' text-justify help'>Help Your Health</h1>
					  <h1 className='text-justify pro'>Problems</h1>
					  <p className="text-justify text">In times like today, your health is very important,
						  especially since the number of COVID-19 cases is
						  increasing day by day, so we are ready to help you
						  with your health consultation
					  </p>
					  <button type="button" className='Try'>Try Free Consultation</button>
					  <div className='row my-5 row-justify-content'>
						  <div className='col col-lg-2 col-md-2 col-sm-2 col-xs-2'>
							  <h3 className='title mr-5'>200<span>+</span></h3>
							  <p className='para'>Active <br /> Doctor</p>
						  </div>
						  <div className='col col-lg-2 col-md-2 col-sm-2 col-xs-2'>
							  <h3 className='title mr-5'>15k<span>+</span></h3>
							  <p className='para'>Active <br /> User</p>
						  </div>
						  <div className='col col-lg-2 col-md-2 col-sm-2 col-xs-2'>
							  <h3 className='title mr-5'>50<span>+</span></h3>
							  <p className='para'>Active <br /> Pharmacy</p>
						  </div>
					  </div>
				  </div>
				  <div className='col  rig'>
					  <div className="rectancle">
						  <img src={doctor} className="doctor " alt="doctor" />
					  </div>
				  </div>
				  <div className='row  mb-5'>
					  <div className='col '>
						  <img src={img1} alt="img1" />
					  </div>
					  <div className='col '>
						  <img src={img1} alt="img1" />
					  </div>
					  <div className='col '>
						  <img src={img1} alt="img1" />
					  </div>
					  <div className='col '>
						  <img src={img1} alt="img1" />
					  </div>
					  <div className='col '>
						  <img src={img1} alt="img1" />
					  </div>
				  </div>
				  <div className="row main">
					  <h1 className='text-center titre-min'>our  <span className='main'>main services</span> <br /> categorie</h1>
				  </div>
				  <div className="container">
					  <div className="row justify-content-lg-center">
						  <div className="col col-lg-2 col-md-4 col-sm-4 col-xs-4  gauche">
							  <div className='font text-center'>
								  <img className=' ' src={doctor1} alt="doctor1" />
							  </div>
							  <div className='text-justify  '>
								  <p className=' '>Chat with doctor</p>
							  </div>
							  <p className='text text-content'>You can connect directly, quickly and easily, and there
								  is no need to doubt the quality of the consultation and treatment offered.</p>
						  </div>
						  <div className="col col-lg-2 col-md-4 col-sm-4 mil">
							  <div className='font text-center'>
								  <img className=' ' src={doctor1} alt="doctor1" />
							  </div>
							  <div className='text-justify  '>
								  <p className=' '>Chat with doctor</p>
							  </div>
							  <p className='text-wrap text-content'>You can connect directly, quickly and easily, and there
								  is no need to doubt the quality of the consultation and treatment offered.</p>
						  </div>
						  <div className="col col-lg-2 col-md-4 col-sm-4 col-xs-4 gauche">
							  <div className='font text-center'>
								  <img className=' ' src={doctor1} alt="doctor1" />
							  </div>
							  <div className='text-justify  '>
								  <p className=' '>Chat with doctor</p>
							  </div>
							  <p className='text-wrap text-content'>You can connect directly, quickly and easily, and there
								  is no need to doubt the quality of the consultation and treatment offered.</p>
						  </div>
					  </div>
				  </div>
			  </div>
		  </div>
		  <div className="container my-5">
			  <div className="row justify-content-lg-center my-5">
				  <div className="col col-lg-4 col-md-6 col-sm-4 docs-left">
					  <img src={docs} alt="docs" className='docs' />
				  </div>
				  <div className="col col-lg-8 col-md-6 col-sm-4 docs-rig ">
					  <h3 className='text-center'>our <span>special services</span></h3>
					  <p className='text-center'>In times like today, your health is very important,<br />
						  especially since the number of COVID-19 cases is <br />
						  increasing day by day, so we are ready to help you <br /> with your health consultation
					  </p>
					  <img src={groupe23} alt="groupe23" />
				  </div>
			  </div>
		  </div>
		  <div className='container-fluid color '>
			  <div className='row'>
				  <div className='col col-lg-6 col-md-6 col-sm-6 my-5'>
					  <div className='text-center'>
						  <h1 className=' text-black'>our Doctor</h1>
						  <h1 className=' text-bel'>Qualified Doctors</h1>
						  <p className='text-center'>Handled directly by general doctors and professional <br />
							  and experienced specialists doctors.</p>
						  <div className='sou'>
							  <img src={groupe132} alt="" />
						  </div>
						  <button type='button' className='text-center my-3'>View All doctor</button>
					  </div>
				  </div>
				  <div className='col col-lg-6 col-md-6 col-sm-6 sou-rig my-5'>
					  <img src={lunete} alt="lune" className='lunete' />
				  </div>
			  </div>
			  <div className="container-fluid blan">
				  <div className="row my-5">
					  <h1 className="text-center">Get <span>started with HaiDoc</span></h1>
					  <p className="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sodales morbi tristique libero <br /> urna sem vitae. Viverra facilisis rhoncus et, nibh nullam vitae laoreet. </p>
					  <button type='button' className='text-center'> Lets Get Started</button>
				  </div>
				  <div className='container-fluid footer'>
					  <div className="row">
						  <div className="col col-lg-2 col-md-4 col-sm-4 col-xs-4">
							  <h6 className='text-center'>Tutorify</h6>
							  <ul className='text-center'>
								  <li className=''>Learn more than just
									  a language
								  </li>
							  </ul>
						  </div>
						  <div className="col col-lg-2 col-md-4 col-sm-4 col-xs-4">
							  <h6 className='text-center'>Tutorify</h6>
							  <ul className='text-center'>
								  <li className=''>Learn more than just
									  a language
								  </li>
								  <li className=''>Learn more than just
									  a language
								  </li>
								  <li className=''>Learn more than just
									  a language
								  </li>
								  <li className=''>Learn more than just
									  a language
								  </li>
							  </ul>
						  </div>
						  <div className="col col-lg-2 col-md-4 col-sm-4 col-xs-4">
							  <h6 className='text-center'>Find Teacher</h6>
							  <ul className='text-center'>
								  <li className=''>Learn more than just
									  a language
								  </li>
								  <li className=''>Learn more than just
									  a language
								  </li>
								  <li className=''>Learn more than just
									  a language
								  </li>
								  <li className=''>Learn more than just
									  a language
								  </li>
							  </ul>
						  </div>
						  <div className="col col-lg-2 col-md-4 col-sm-4 col-xs-4">
							  <h6 className='text-center'>Lesson</h6>
							  <ul className='text-center'>
								  <li className=''>Learn more than just
									  a language
								  </li>
								  <li className=''>Learn more than just
									  a language
								  </li>
								  <li className=''>Learn more than just
									  a language
								  </li>
								  <li className=''>Learn more than just
									  a language
								  </li>
							  </ul>
						  </div>
						  <div className="col col-lg-2 col-md-2 col-sm-2	col-xs-2">
							  <h6 className='text-center'>Tutorify</h6>
							  <ul className='text-center'>
								  <li className=''>Learn more than just
									  a language
								  </li>
								  <li className=''>Learn more than just
									  a language
								  </li>
								  <li className=''>Learn more than just
									  a language
								  </li>
							  </ul>
						  </div>
					  </div>
				  </div>
			  </div>
		  </div>
		
		

	</>
  )
}

export default Header